<?php include('init.php'); ?>

<div class="container">
	<h1 class="text-center">Show Categories</h1>
    
    	<div class="row">
	    	<?php $items = getItems('cat_id',$_GET['pageid']);
			if(count($items) > 0){
			 foreach($items as $item){ ?>
			 
			 <div class="co-sm-6 col-md-3">
	    		<div class="card item-box" style="width: 18rem;">
	    		    <span class="price-tag"><?php echo $item['price']?></span>
				    <img class="card-img-top" src="download.jpg" alt="Card image cap">
				    <div class="card-body">
				      <h3 class="card-title"><a href='items.php?id=<?php echo $item['item_id'];?>'><?php echo $item['name'];?></a></h3>
				      <p class="card-text">
							<?php	echo $item['description'] ."<br/>";?>
					  </p>
					  <div><span class="float-right"><small class="text-muted"><?php echo $item['add_date'];?></small></span></div>
				   </div>
	    	      </div>
	    	    </div>
						 <?php }  }else{
						    	echo "ther is no item for that category ";
						    }?>
			    
        </div>
	
	
	
</div>

 <?php include('includes/templates/footer.php');?>