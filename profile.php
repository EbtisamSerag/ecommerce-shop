<?php

 session_start();
 $pageTitle="Home Page";
 include('init.php');
 if(isset($_SESSION['client'])){
 	$q = $con->prepare("SELECT * FROM users WHERE username=?");
 	$q->execute(array($clientsession));
 	$user = $q->fetch();
?>
<h1 class="text-center">My Profile</h1>
<div class="info block">
 	<div class="container">
 		<div class="card ">
		  <div class="card-header bg-primary">
		    My Infromation
		  </div>
		  <div class="card-body">
            <ul class="list-group">
			  <li class="list-group-item"><i class="fa fa-unlock-alt "></i> <span>Name</span> : <?php echo $user['username'];?></li>
			  <li class="list-group-item"><i class="fa fa-envelope"></i> <span>email</span> : <?php echo $user['username'];?> </li>
			  <li class="list-group-item"><i class="fa fa-user"></i> <span>Full Name</span> :  <?php echo $user['fullname'];?></li>
			  <li class="list-group-item"><i class="fa fa-calendar "></i> <span>Favourite</span> : <?php  ?></li>
			</ul>
			<a href="" class="btn btn-secondary edit-profile">Edit Profile</a>

		  </div>
		</div>
 	</div>
 </div>

 <div class="ads block" id=myitem>
 	<div class="container">
 		<div class="card ">
		  <div class="card-header bg-primary">
		    Latest Items
		  </div>
		  <div class="card-body">
             <div class="row">
	    	<?php $items = getItems('member_id',$user['user_id'],'0');
			if(count($items) > 0){
			 foreach($items as $item){ ?>
			 
			 <div class="co-sm-6 col-md-4">
	    		<div class="card item-box" style="width: 18rem;">
	    			<?php if($item['approve'] == 0){?>
	    			<span class="approve">Need to approve</span>
	    		<?php }?>
	    		    <span class="price-tag"><span>$</span><?php echo $item['price']?></span>
				    <img class="card-img-top" src="download.jpg" alt="Card image cap">
				    <div class="card-body">
				      <h3 class="card-title"><a href='items.php?id=<?php echo $item['item_id'];?>'><?php echo $item['name'];?></a></h3>
				      <p class="card-text">
							<?php echo $item['description'];?>
					  </p>
					  <div><span class="float-right"><small class="text-muted"><?php echo $item['add_date'];?></small></span></div>
				   </div>
	    	      </div>
	    	    </div>
						 <?php }  }else{
						    	echo "ther is no items to show >> <a href='newads.php'> Create item</a> ";
						    }?>
			    
        </div>
		  </div>
		</div>
 	</div>
 </div>

  <div class="commet block">
 	<div class="container">
 		<div class="card ">
		  <div class="card-header bg-primary">
		    Latest Comments
		  </div>
		  <div class="card-body">
             <?php
            $q = $con->prepare("SELECT comment FROM comments WHERE user_id=?");
		 	$q->execute(array($user['user_id']));
		 	$comments = $q->fetchAll();
		 	if(!empty($comments)){
		 	foreach($comments as $comment){
		 		echo $comment['comment']."<br/>";
		 	}
		    }else{
		    	echo "ther is no comment ";
		    }

             ?>
		  </div>
		</div>
 	</div>
 </div> 

<?php 
}else {
	header('location:login.php');
	eixt();
}
include('includes/templates/footer.php');?>