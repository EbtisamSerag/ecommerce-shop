<?php

 //title function
  function getTitle()
  {
    global $pageTitle;
    if(isset($pageTitle))
    {
      echo "$pageTitle";
    }else{
      echo "default";
    }
  }
  //get all categories all items all users... function
  function getAll($tbl,$wher=null){
    global $con;
    $wher = isset($wher) == null? '':$wher;
    $q = $con->prepare("SELECT * FROM $tbl $wher");
    $q->execute();
    $all = $q->fetchAll();
    return $all;
  }
  //get items where cat_id = $id and approve = 1
  function getItems($where,$value,$approve =null){
    global $con;
    if($approve == null){
      $appr = 'AND approve=1';
    }else{
      $appr = '';
    }
    $q = $con->prepare("SELECT * FROM items WHERE $where = ? $appr ORDER BY item_id DESC");
    $q->execute(array($value));
    $items = $q->fetchAll();
    return $items;
  }
  //function to get user status
  function checkStatus($user){
    global $con;
    $q = $con->prepare("SELECT * FROM users WHERE username = ? AND reg_status = 0");
    $q->execute(array($user));
    $count = $q->rowCount();
    return $count;
  }
  //
   function checkItem($select ,$from ,$value)
   {  
      global $con;
      $q = $con->prepare("SELECT $select FROM $from WHERE $select =?");
      $q->execute(array($value));
      $count = $q->rowCount();
      return $count;
   }
?>