<?php

 session_start();
 $pageTitle="Show Items";
 include('init.php');

        $id =intval($_GET['id']) ;
 	    $itemId = isset($_GET['id']) && is_numeric($_GET['id'])?  $id:0;
        $query = $con->prepare("SELECT items.* ,categories.name AS cat_name ,users.username
                                FROM items 
                                INNER JOIN categories ON items.cat_id = categories.id
                                INNER JOIN users ON items.member_id = users.user_id
                                WHERE item_id=?
                                AND approve =1");
        $query->execute(array($itemId));
        $row = $query->fetch();
        if($query->rowCount()>0){
?>
<h1 class="text-center"><?php echo $row['name']?></h1>
 <div class="container">
 	<div class="row">
 		<div class="col-md-3">
 			<img src="download.jpg">
 		</div>
 		<div class="col-md-9">
 			<ul class="list-group">
			  <li class="list-group-item list-group-item-secondary"><h2><?php echo $row['name']?></h2></li>
			  <li class="list-group-item"><?php echo $row['description']?></li>
			  <li class="list-group-item"><i class="fa fa-money-bill-alt" ></i>
			  	<span>Price</span> : <?php echo $row['price']?></li>
			  <li class="list-group-item"><i class="fa fa-building"></i>
			  	</i><span>Made in</span> : <?php echo $row['country_made']?></li>
			  <li class="list-group-item"><i class="fa fa-tags"></i>
			  	<span>Category Name</span> : <a href="categories.php?pageid=<?php echo $row['cat_id']?>"><?php echo $row['cat_name']?></a></li>
			  <li class="list-group-item"><i class="fa fa-user"></i>
			  	<span>Added bu</span> : <a href="#"><?php echo $row['username']?></a></li>
			  <li class="list-group-item"><i class="fa fa-calendar"></i>
			  	<?php echo $row['add_date']?></li>

			</ul>

 		</div>

 	</div>
 	<hr>
 	<?php if(isset($_SESSION['client'])){ ?>
 	<div class="row">
 		<div class="offset-md-3 col-md-9">
 			<h3>Add your comment</h3>
 			<form action="?id=<?php echo $row['item_id'] ?>" method="POST">
 				<div class="form-group">
				    <textarea  class="form-control" name="comment" required></textarea> 
			    </div>
			    <div class="form-group">
				    <input type="submit" class="btn btn-primary" value="Comment">
		        </div>
 			</form>
 			<?php
 			if($_SERVER['REQUEST_METHOD']=='POST'){
 				$comment   = filter_var($_POST['comment'], FILTER_SANITIZE_STRING);
 				$user_id   = $_SESSION['client_id'];
 				$itemid    = $row['item_id'];
 				if(!empty($comment)){
 					$query = $con->prepare("insert into comments(comment,status,comm_date,item_id,user_id) values(?,0,now(),?,?)");
	    		    $inset_success = $query->execute(array($comment,$itemid,$user_id));
	    		if($inset_success){
	    		echo  "<div class='alert alert-success text-center'>Comment Added </div>";
                }

 				}

 			  
 			}
 			?>
 		</div>
 	</div>
    <?php }else{
        echo "<a href='login.php'>Login</a> OR <a href='login.php'>SignUp</a> to Add comment...";
    }?>
 <hr>
 			<?php
 			$query = $con->prepare("SELECT comments.* ,users.username
                                FROM comments 
                                INNER JOIN users ON comments.user_id = users.user_id
                                WHERE item_id=? AND status='1'");
            $query->execute(array($itemId));
            $rows = $query->fetchAll();

            if($query->rowCount()>0){
            	foreach ($rows as $comment) { ?>
            		<div class="comment-box">
	            		<div class="row">
					 		<div class="col-md-2 text-center block">
					 			<img src="download.jpg" class="img-fluid rounded-circle">
					 			<h5><?php echo $comment['username']?></h5></div> 
					 		<div class="col-md-10 comm"><?php echo $comment['comment']?></div>
	                    </div>
                    </div>
                    <hr>
           <?php    }
            }
        	?>
    
 </div>

<?php 
}else {
	echo "<div class='alert alert-danger'>there is no such id or this item need to Approve</div>";
}
include('includes/templates/footer.php');?>