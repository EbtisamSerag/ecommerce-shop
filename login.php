<?php 
session_start();
$pageTitle="Login";
   if(isset($_SESSION['client'])){
   	header('location:index.php');
   }

include('init.php'); 

 if($_SERVER['REQUEST_METHOD'] == 'POST'){
       if(isset($_POST['login'])){//login
          	$user       = $_POST['username'];
          	$password   = $_POST['password'];
          	$hashedpass = sha1($password);

          	$query=$con->prepare("select user_id,username,password from users where username=? and password=?");
          	$query->execute(array($user,$hashedpass));
          	$count=$query->rowcount();
            $usr =$query->fetch();

          	if($count > 0)
          	{
          		$_SESSION['client'] = $user; //register session by name
              $_SESSION['client_id'] = $usr['user_id']; //register session by id
              //register session by name
          		header('location: index.php');//rediect to home page
          		exit();
          	}
      }else{//signup
            
            $username     = $_POST['username'];
            $email        = $_POST['email'];
            $password     = $_POST['password'];
            $con_password = $_POST['con_password'];
            $email        = $_POST['email'];


            $formerrors = array();
            if(isset($_POST['username'])){
               $flt_username = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
               if(empty($flt_username)){
                  $formerrors[] = "user name cannt be <strong>empty</strong>";
               }
            }
            if(isset($_POST['password']) && isset($_POST['con_password'])){
               if(empty($_POST['password'])){
                  $formerrors[] = "password cannt be <strong>empty</strong>";
               }
               $pass     = sha1($_POST['password']);
               $con_pass = sha1($_POST['con_password']);
               if ($pass != $con_pass){
                  $formerrors[] = "password must be <strong>matched</strong>";
               }
            }
            if(isset($_POST['email'])){
               $flt_email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
               if($flt_email != true){
                  $formerrors[] = "email must be <strong>valid</strong>";
               }
            }
            if(empty($formerrors)){
               $check = checkItem('username' ,'users' ,$username);
               if($check == 1){
                  $formerrors[] = "username is <strong>exist</strong>";
               }else{
                   $query = $con->prepare("INSERT INTO users (username,password,email) VALUES(?,?,?)");
                   $query->execute(array($username,$password,$email));
                   echo "<div class='alert alert-success text-center'>Register Done</div>";

               }
 
            }
      }
 }


    ?>
   <div class="container login-page">
   	<h1 class="text-center"><span class="selected" data-class="login">Login</span> | <span data-class="signup">SignUp</span></h1>
   	<!-- login form -->
   	<form class="login" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
   		<div class="form-group">
   		   <input class="form-control" type="text" name="username" 
   		placeholder="enter your user name" autocomplete="off" required>
     	</div>
     	<div class="form-group">
   		   <input class="form-control" type="password" name="password" 
   		placeholder="enter your password" autocomplete="new-password" required>
   	    </div>
   	    <div class="form-group">
   		   <input class="btn btn-primary btn-block" type="submit" name="login" value="Login">
   	    </div>
   	</form>
    <!-- signup form -->
   	<form class="signup" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
   		<div class="form-group">
   		   <input class="form-control" type="text" name="username" 
   		placeholder="enter your user name" autocomplete="off" required>
   	    </div>
   	    <div class="form-group">
   		   <input class="form-control" type="password" name="password" 
   		placeholder="enter your password" autocomplete="new-password" required>
   	    </div>
   	    <div class="form-group">
   		   <input class="form-control" type="password" name="con_password" 
   		placeholder="confirm your password" autocomplete="new-password" required>
   	    </div>
   	    <div class="form-group">
   		   <input class="form-control" type="email" name="email" placeholder="enter avalid Email" required>
   	    </div>
   	    <div class="form-group">
   		   <input class="btn btn-success btn-block" type="submit" name="signup" value="SignUp">
   	    </div>
   	</form>
      <div class="errors text-center">
         <?php if(!empty($formerrors)){
         foreach($formerrors as $error){
            echo "<div class='text-danger'>".$error."</div>";
         } 
         }?>
      </div>
   </div>


   
<?php include('includes/templates/footer.php');?>