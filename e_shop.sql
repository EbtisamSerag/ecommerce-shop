-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2019 at 10:10 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent_id` int(11) NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `visibility` tinyint(4) NOT NULL DEFAULT '0',
  `allow_comment` tinyint(4) NOT NULL DEFAULT '0',
  `allow_ads` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `parent_id`, `ordering`, `visibility`, `allow_comment`, `allow_ads`) VALUES
(1, 'Computers sys', 'computers items', 0, 0, 0, 0, 0),
(2, 'electronics', 'electronics items', 0, 1, 0, 0, 0),
(3, 'toys', 'toys items', 2, 0, 0, 1, 0),
(4, 'baby', 'baby items', 5, 0, 0, 0, 0),
(5, 'beauty and health', 'beauty and health', 0, 0, 0, 0, 0),
(6, 'test sub', 'test subtest subtest sub', 2, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comm_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `comm_date` date NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comm_id`, `comment`, `status`, `comm_date`, `item_id`, `user_id`) VALUES
(1, 'ecgggzfgchh', 0, '2019-03-20', 2, 5),
(2, 'vccomment ', 0, '2019-03-20', 2, 5),
(3, 'lmfkg jnuioyoui jkwymiemiyca ', 0, '2019-03-20', 2, 5),
(4, 'rcrrrrrrrrrrrrrrrrrrrrr', 1, '2019-03-08', 7, 13),
(5, 'rcrrrrrrrrrrrrrrrrrrrrr', 0, '2019-03-08', 7, 13),
(6, 'rcrrrrrrrrrrrrrrrrrrrrr', 0, '2019-03-08', 7, 13),
(7, 'rcrrrrrrrrrrrrrrrrrrrrr', 1, '2019-03-08', 7, 13),
(8, 'rcrrrrrrrrrrrrrrrrrrrrr', 0, '2019-03-08', 7, 13),
(9, 'rcrrrrrrrrrrrrrrrrrrrrr', 1, '2019-03-08', 7, 13),
(10, 'teyyhjgh', 0, '2019-03-08', 7, 13),
(11, 'teyyhjgh', 0, '2019-03-08', 7, 13),
(12, 'teyyhjgh', 0, '2019-03-08', 7, 5),
(13, 'teyyhjgh', 0, '2019-03-08', 7, 5),
(14, 'fffffffffffffffffffffffffffffffffffffffffffffffffffff', 1, '2019-03-08', 7, 5);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `add_date` date NOT NULL,
  `country_made` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `rating` smallint(6) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `approve` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `name`, `description`, `price`, `add_date`, `country_made`, `image`, `status`, `rating`, `cat_id`, `member_id`, `approve`) VALUES
(1, 'electronic monitor', 'electronic monitor', 2220, '2019-03-06', 'europ', '', '1', 0, 2, 1, 1),
(2, 'tv', 'tv with high quality', 2220, '2019-03-06', 'europ', '', '1', 0, 2, 12, 1),
(3, 'ball', 'big ball', 2220, '2019-03-06', 'europ', '', '2', 0, 3, 13, 0),
(4, 'mouse', 'mouse mouse', 2343, '2019-03-07', 'china', '', '1', 0, 1, 5, 0),
(5, 'mouse', 'mouse mouse', 2343, '2019-03-07', 'china', '', '1', 0, 1, 5, 1),
(6, 'mouse', 'mouse mouse', 2343, '2019-03-07', 'china', '', '1', 0, 1, 5, 1),
(7, 'ball', 'big ball', 2220, '2019-03-06', 'europ', '', '2', 0, 1, 13, 0),
(8, 'electronic monitor', 'electronic monitor', 2220, '2019-03-06', 'europ', '', '1', 0, 1, 5, 0),
(9, 'mouse', 'mouse mouse  categoryther is no item for that categoryther is no item for that categoryther is no item for that categoryther is no item for that categoryther is no i', 2343, '2019-03-07', 'china', '', '1', 0, 2, 5, 1),
(10, 'ccedd', 'dvasvvv rfrg erwwwttrggdffs', 5255, '2019-03-10', 'egy', '', '2', 0, 2, 5, 0),
(11, 'ccedd', 'dvasvvv rfrg erwwwttrggdffs', 5255, '2019-03-10', 'egy', '', '2', 0, 2, 5, 0),
(12, 'cfjniytre343', 'sxj jmhkh ewhmp ioj', 3654, '2019-03-10', 'europ', '', '1', 0, 5, 5, 0),
(13, 'cfjniytre343', 'sxj jmhkh ewhmp ioj', 3654, '2019-03-10', 'europ', '', '1', 0, 5, 5, 0),
(14, 'cfjniytre343', 'sxj jmhkh ewhmp ioj', 3654, '2019-03-10', 'europ', '', '1', 0, 5, 5, 0),
(15, 'cfjniytre343', 'sxj jmhkh ewhmp ioj', 3654, '2019-03-10', 'europ', '', '1', 0, 5, 5, 0),
(16, 'cfjniytre343', 'sxj jmhkh ewhmp ioj', 3654, '2019-03-10', 'europ', '', '1', 0, 5, 5, 0),
(17, 'cfjniytre343', 'sxj jmhkh ewhmp ioj', 3654, '2019-03-10', 'europ', '', '1', 0, 5, 5, 0),
(18, 'jjhjihcr', 'rcpwoj kmurewpe[w],m', 6542482, '2019-03-10', 'europ', '', '1', 0, 2, 5, 0),
(19, 'jjhjihcr', 'rcpwoj kmurewpe[w],m', 6542482, '2019-03-10', 'europ', '', '1', 0, 2, 5, 0),
(20, 'jjhjihcr', 'rcpwoj kmurewpe[w],m', 6542482, '2019-03-10', 'europ', '', '1', 0, 2, 5, 0),
(21, 'jjhjihcr', 'rcpwoj kmurewpe[w],m', 6542482, '2019-03-10', 'europ', '', '1', 0, 2, 5, 0),
(22, 'testdd', 'testddtestdd testdd', 45555, '2019-03-10', 'china', '', '2', 0, 1, 5, 0),
(23, 'lkppojfkd', 'myn iuehiu', 654, '2019-03-10', 'cfsd', '', '1', 0, 3, 5, 0),
(24, 'lkppojfkd', 'myn iuehiu', 654, '2019-03-10', 'cfsd', '', '1', 0, 3, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `trust_status` int(11) NOT NULL DEFAULT '0',
  `reg_status` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `fullname`, `image`, `group_id`, `trust_status`, `reg_status`, `date`) VALUES
(1, 'admin', 'f10e2821bbbea527ea02200352313bc059445190', 'admin@test.com', 'administrator', '', 1, 0, 1, '0000-00-00'),
(5, 'asd', 'f10e2821bbbea527ea02200352313bc059445190', 'admin123@test.com', 'ebtisam nasr', '', 0, 0, 0, '0000-00-00'),
(11, 'ahmed', 'f10e2821bbbea527ea02200352313bc059445190', 'ahmed@test.com', 'ahmed alaa', '', 0, 0, 0, '0000-00-00'),
(12, 'alaa', '7e240de74fb1ed08fa08d38063f6a6a91462a815', 'alaa123@test.com', 'alaa ali', '', 0, 0, 0, '2019-03-04'),
(13, 'walaa', 'f10e2821bbbea527ea02200352313bc059445190', 'walaa@test.com', 'walaa ahmed', '', 0, 0, 0, '2019-03-05'),
(14, 'qwe', 'qwe', 'qwe@test.com', '', '', 0, 0, 0, '0000-00-00'),
(15, 'zxc', 'zxc', 'zxc@test.com', 'wzdoihmoidsa', '', 0, 0, 0, '0000-00-00'),
(16, 'mohamed', 'f10e2821bbbea527ea02200352313bc059445190', 'mohamed@test.com', 'mohamed emad', '24655_Naruto Shippuden Uzumaki Naruto salute.jpg', 0, 0, 1, '2019-03-10'),
(17, 'saad', 'f10e2821bbbea527ea02200352313bc059445190', 'saad@test.com', 'saad mahmoud', '19339_Naruto Shippuden Uzumaki Naruto salute.jpg', 0, 0, 1, '2019-03-10'),
(18, 'elsa', 'f10e2821bbbea527ea02200352313bc059445190', 'elsa@test.com', 'elsa daldd', '67360_images (3).jpg', 0, 0, 1, '2019-03-10'),
(19, 'aljllaa', 'f10e2821bbbea527ea02200352313bc059445190', 'aljllaa3@test.com', 'aljllaa jngnss', '97301_images (1).jpg', 0, 0, 1, '2019-03-10'),
(20, 'mona', 'f10e2821bbbea527ea02200352313bc059445190', 'mona@test.com', 'mona mohamed', '5146_images.jpg', 0, 0, 1, '2019-03-10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comm_id`),
  ADD KEY `fk_item_id` (`item_id`),
  ADD KEY `fk_member_id` (`user_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `FK_userid` (`member_id`),
  ADD KEY `FK_catid` (`cat_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_item_id` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_member_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `FK_catid` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_userid` FOREIGN KEY (`member_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
