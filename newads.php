<?php

 session_start();
 $pageTitle="New Item";
 include('init.php');
 if(isset($_SESSION['client'])){
 	if($_SERVER['REQUEST_METHOD'] == 'POST'){

       $user_id    = $_SESSION['client_id'];
      $formerrors  = array();
	 	$title     = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
	 	$desc      = filter_var($_POST['desc'], FILTER_SANITIZE_STRING);
	 	$price     = filter_var($_POST['price'], FILTER_SANITIZE_NUMBER_INT);
	 	$coun_made = filter_var($_POST['country_made'], FILTER_SANITIZE_STRING);
	 	$status    = filter_var($_POST['status'], FILTER_SANITIZE_NUMBER_INT);
	 	$cat_id    = filter_var($_POST['cat_id'], FILTER_SANITIZE_NUMBER_INT);
       if(empty($title)){
       	$formerrors[] = "title cannt be <strong>empty</strong>";
       }
       if(empty($desc)){
       	$formerrors[] = "description cannt be <strong>empty</strong>";
       }
       if(empty($price)){
       	$formerrors[] = "price cannt be <strong>empty</strong>";
       }
       if(empty($coun_made)){
       	$formerrors[] = "country made cannt be <strong>empty</strong>";
       }
       if(empty($cat_id)){
       	$formerrors[] = "category cannt be <strong>empty</strong>";
       }
       if(empty($formerrors))
            {

            	$query = $con->prepare("insert into items(name,description,price,country_made,status,add_date,cat_id,member_id) values(?,?,?,?,?,now(),?,?)");
	    		$ins_success = $query->execute(array($title,$desc,$price,$coun_made,$status,$cat_id,$user_id));
          if($ins_success){ 
            $success_msg = "Insert Done";
 	    		}
            }
    }

?>
<h1 class="text-center">Create New Item</h1>
<div class="info block">
 	<div class="container">
 		<div class="card ">
		  <div class="card-header bg-primary">
		    My Infromation
		  </div>
		  <div class="card-body">
		  	<div class="row">
		  		<div class="col-md-8">
		  			
		  		<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                <div class="form-group row ">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10 col-md-9">
                        <input type="text" name="name" class="form-control live-name" placeholder="enter Item name" required >
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10 col-md-9">
                        <textarea  name="desc" class="form-control live-desc" placeholder="enter description of item" required ></textarea>
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10 col-md-9">
                        <input type="text" name="price" class="form-control live-price" placeholder="enter ordering price of item" required>
                    </div>
                    </div>
                    <div class="form-group row ">
                    <label class="col-sm-2 control-label">Country</label>
                    <div class="col-sm-10 col-md-9">
                        <input type="text" name="country_made" class="form-control" placeholder="enter country of made" required  >
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10 col-md-9">
                            <select class="custom-select" name="status">
							  <option selected>...</option>
							  <option value="1">NEW</option>
							  <option value="2">LIKE NEW</option>
							  <option value="3">OLD</option>
							</select>
                    </div>
                    </div>

                    <div class="form-group row">
                    <label class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-10 col-md-9">
                            <select class="custom-select" name="cat_id">
							  <option selected>...</option>
							  <?php 
							  foreach (getAll('categories') as $category)
							  	echo "<option value='".$category['id']."'>".$category['name']."</option>";
							  ?>
							</select>
                    </div>
                    </div>                    
                    <div class="form-group row ">                 
                    <div class="offset-sm-2 col-sm-10 ">
                        <input type="submit" value="Add Ads" class="btn btn-primary">
                    </div>
                    </div>
                
            </form>

		  		</div>
		  		<div class="col-md-4 live-preview">
	    		<div class="card item-box" style="width: 18rem;">
	    		    <span class="price-tag">$</span>
				    <img class="card-img-top" src="download.jpg" alt="Card image cap">
				    <div class="card-body">
				      <h3 class="card-title"></h3>
				      <p class="card-text">
					  </p>
				   </div>
	    	      </div>
	    	    </div>
	        </div>
            <?php 
            if(!empty($formerrors)){
            	foreach ($formerrors as $error) {
            		echo '<div class="alert alert-danger">'.$error.'</div>';
            	}
            }
            if(isset($success_msg)){
                echo  "<div class='alert alert-success text-center'> ".$success_msg." </div>";
                }?>
		  </div>
		</div>

 	</div>
 </div>
 

<?php 
}else {
	header('location:login.php');
	eixt();
}
include('includes/templates/footer.php');?>