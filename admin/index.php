<?php

session_start();
$nonavbar="";
$pageTitle="Login";

 include('init.php');
 
  
  //chec if usr coming from http post request
 if($_SERVER['REQUEST_METHOD'] == 'POST'){
 	$user = $_POST['user'];
 	$password = $_POST['pass'];
 	$hashedpass = sha1($password);

    //check if usr exist in database
 	$query=$con->prepare("select user_id,username,password from users where username=? and password=? and group_id=1 LIMIT 1");
 	$query->execute(array($user,$hashedpass));
 	$row=$query->fetch();
 	$count=$query->rowcount();
 	
 	if($count > 0)
 	{
 		$_SESSION['user'] = $user; //register session by name
 		$_SESSION['id']   = $row['user_id']; //register session by id
 		header('location: home.php');//rediect to home page
 		exit();
 	}

 }



?>
<!-- login form -->
<form class="login" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
	<h4 class="text-center">Admin Login</h4>
	<input class="form-control" type="text" name="user" placeholder="username" autocomplete="off" />
	<input class="form-control" type="password" name="pass" placeholder="password" autocomplete="new-password" />
	<input class="btn btn-primary btn-block" type="submit" value="login" name="adduser" />
</form>



<?php
 include('includes/templates/footer.php');
?>