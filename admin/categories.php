<?php 
/*
===============================
**manage categories page 
**you can [add,edit,delete]
===============================
*/
session_start();
$pageTitle="categories";
if(isset($_SESSION['user']))
{
	
	include 'init.php';

	$page =isset($_GET['page'])? $_GET['page']:'manage';
	if($page=='manage'){ 
      
       $query = $con->prepare("SELECT * from categories where parent_id =0");
        $query->execute();
        $cats = $query->fetchAll();
        ?>
        <h1 class="text-center">Manage Categories</h1>
        <div class="container">
            <table class="table main-table table-bordered cat">
              <thead class="thead-dark">
                
                <tr>
                  <th scope="col">#Id</th>
                  <th scope="col">Name</th>
                  <th scope="col">Description</th>
                  <th scope="col">Subcategories</th>
                  <th scope="col">Ordering</th>
                  <th scope="col">Visibility</th>
                  <th scope="col">Allow Comment</th>
                  <th scope="col">Allow Ads</th>                  
                  <th scope="col">Controll</th>

                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($cats as $cat) {
                ?>
                <tr>
                  <th scope="row"><?php echo $cat['id']?></th>
                  <td><?php echo $cat['name']?></td>
                  <td><?php echo $cat['description']?></td>
                  <td>
                    <?php
                    $query = $con->prepare("SELECT * from categories where parent_id =? limit 1");
                    $query->execute(array($cat['id']));
                    $cate = $query->fetch(); 
                  if(!empty($cate)){
                    echo'
                    <a href="?page=subcategories&id='. $cate['parent_id'].'&name='. $cat['name'] .'">
                    <i class="fa fa-table"></i> Display Sub</a>';
                   }
                   ?>
                   </td>
                  <td><?php echo $cat['ordering']?></td>
                  <?php if($cat['visibility'] == 0) {?>
                  <td><sapn class="active">Active</sapn></td>
                 <?php }else{
                    echo "<td><sapn class='not-active'>Not Active</sapn></td>"; }
                    if($cat['allow_comment'] == 0) {?>
                    <td><sapn class="active">Active</sapn></td>
                  <?php }
                  else{
                    echo "<td><sapn class='not-active'>Not Active</sapn></td>"; }
                   if($cat['allow_ads'] == 0) {?>
                   <td><sapn class="active">Active</sapn></td>
                  <?php }
                  else{
                    echo "<td><sapn class='not-active'>Not Active</sapn></td>"; }?>

                  <td><a href="?page=edit&id=<?php echo $cat['id'] ?>" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>
                    <a href="?page=delete&id=<?php echo $cat['id'] ?>" class="btn btn-danger confirm"><i class="fa fa-times"></i> Delete</a>

                </tr>
            <?php }?>
              </tbody>
            </table>
            <a href="?page=add" class="btn btn-primary"><i class="fa fa-plus"></i> New Category</a>
        </div>

     <?php
     }elseif($page=='subcategories'){ 
      
        $id  = intval($_GET['id']) ;
        $cat = $_GET['name'] ;
        $parentId = isset($_GET['id']) && is_numeric($_GET['id'])?  $id:0;
        $cats =getAllform('*','categories','where parent_id ='.$parentId ,'','id','DESC');
        
        ?>

        <h1 class="text-center">[<?php echo $cat; ?>] Subcategories</h1>
        <div class="container">
            <table class="table main-table table-bordered cat">
              <thead class="thead-dark">
                
                <tr>
                  <th scope="col">#Id</th>
                  <th scope="col">Name</th>
                  <th scope="col">Description</th>
                  <th scope="col">Ordering</th>
                  <th scope="col">Visibility</th>
                  <th scope="col">Allow Comment</th>
                  <th scope="col">Allow Ads</th>                  
                  <th scope="col">Controll</th>

                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($cats as $cat) {
                ?>

                <tr>
                  <th scope="row"><?php echo $cat['id']?></th>
                  <td><?php echo $cat['name']?></td>
                  <td><?php echo $cat['description']?></td>
                  <td><?php echo $cat['ordering']?></td>
                  <?php if($cat['visibility'] == 0) {?>
                  <td><sapn class="active">Active</sapn></td>
                 <?php }else{
                    echo "<td><sapn class='not-active'>Not Active</sapn></td>"; }
                    if($cat['allow_comment'] == 0) {?>
                    <td><sapn class="active">Active</sapn></td>
                  <?php }
                  else{
                    echo "<td><sapn class='not-active'>Not Active</sapn></td>"; }
                   if($cat['allow_ads'] == 0) {?>
                   <td><sapn class="active">Active</sapn></td>
                  <?php }
                  else{
                    echo "<td><sapn class='not-active'>Not Active</sapn></td>"; }?>

                  <td><a href="?page=edit&id=<?php echo $cat['id'] ?>" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>
                    <a href="?page=delete&id=<?php echo $cat['id'] ?>" class="btn btn-danger confirm"><i class="fa fa-times"></i> Delete</a>

                </tr>
            <?php }?>
              </tbody>
            </table>
            <a href="?page=add" class="btn btn-primary"><i class="fa fa-plus"></i> New Category</a>
        </div>
    <?php
    }elseif($page =='add'){ ?>
        
        <h1 class="text-center">Add Category</h1>
        <div class="container">
            <form action="?page=insert" method="POST">
                <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="name" class="form-control" placeholder="enter category name" required>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10 col-md-6">
                        <textarea  name="desc" class="form-control" placeholder="enter description of category" ></textarea>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">ordering</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="order" class="form-control" placeholder="enter ordering number of category" >
                    </div>
                    </div>

                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Parent</label>
                    <div class="col-sm-10 col-md-6">
                            <select class="custom-select" name="parent_id">
                              <option value="0">...</option>
                              <?php 
                              $cats=getAllform('*','categories','where parent_id = 0','','id','ASC');
                              
                              foreach($cats as $cat){
                                echo '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';
                              }
                              ?>
                            </select>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Visible</label>
                    <div class="col-sm-10 col-md-6">
                        <div>
                            <input id="vis-yes" type="radio" name="visible" value="0" checked>
                            <label for="vis-yes">Yes</label>
                        </div>
                        <div>
                            <input id="vis-no" type="radio" name="visible" value="1">
                            <label for="vis-no">No</label>
                        </div>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">AllowComment</label>
                    <div class="col-sm-10 col-md-6">
                        <div>
                            <input id="com-yes" type="radio" name="comment" value="0" checked>
                            <label for="com-yes">Yes</label>
                        </div>
                        <div>
                            <input id="com-no" type="radio" name="comment" value="1">
                            <label for="com-no">No</label>
                        </div>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">AllowAds</label>
                    <div class="col-sm-10 col-md-6">
                        <div>
                            <input id="ads-yes" type="radio" name="ads" value="0" checked>
                            <label for="ads-yes">Yes</label>
                        </div>
                        <div>
                            <input id="ads-no" type="radio" name="ads" value="1">
                            <label for="ads-no">No</label>
                        </div>
                    </div>
                    </div>
                    <div class="form-group row btn-lg">                 
                    <div class="offset-sm-2 col-sm-10 offset-md-4">
                        <input type="submit" value="Add Category" class="btn btn-primary">
                    </div>
                    </div>
                
            </form>
        </div>


    <?php 
    }elseif($page== 'insert'){

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            echo "<div class='container'>
                  <h1 class='text-center'>Add Members</h1>";
            $name       = $_POST['name'];
            $desc       = $_POST['desc'];
            $order      = $_POST['order'];
            $parentid   = $_POST['parent_id'];
            $visible    = $_POST['visible'];
            $allow_comm = $_POST['comment'];
            $allow_ads  = $_POST['ads'];


            $formerrors = array();
            if(empty($name))
            {
                $formerrors[] = "name cannt be <strong>empty</strong>.";
            }

            foreach($formerrors as $error)
            {
               echo "<div class='alert alert-danger' >".$error."</div>";
            }
            if(empty($formerrors))
            {
                $check = checkItem('name','categories',$name);
                if($check == 1)
                {
                    $msg = "<div class='alert alert-danger'>sorry this username is exist </div>";
                    redirectHome($msg,'back');
                }else{
                    $query = $con->prepare("insert into categories(name,description,parent_id,visibility,ordering,allow_comment,allow_ads) values(?,?,?,?,?,?,?)");
                    $query->execute(array($name,$desc,$parentid,$order,$visible,$allow_comm,$allow_ads));
                    $msg = "<div class='alert alert-success'>Insert Done </div>";
                    redirectHome($msg,'back');
              }
            }
        }else{
            echo "<div class='container'>";
            $msg = "<div class='alert alert-danger'>you cannt view that page!</div>";
            redirectHome($msg,'back');
            echo "</div>";
        }

		
	}elseif($page=='edit'){//edit page

    $id =intval($_GET['id']) ;
    $catId = isset($_GET['id']) && is_numeric($_GET['id'])?  $id:0;
    $query =$con->prepare("select * from categories where id =? limit 1");
    $query->execute(array($catId));
    $row =$query->fetch();
    if($query->rowCount()>0){?>
        
         <h1 class="text-center">Edit Category</h1>
        <div class="container">
            <form action="?page=update" method="POST">
                <input type="hidden" name="catid" value="<?php echo $row['id']?>">
                <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="name" class="form-control" value="<?php echo $row['name']?>" required>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10 col-md-6">
                        <textarea  name="desc" class="form-control"placeholder="enter description of category" ><?php echo $row['description']?></textarea>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">ordering</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="order" class="form-control" placeholder="enter ordering number of category"  value="<?php echo $row['ordering']?>" >
                    </div>
                    </div>

                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Parent</label>
                    <div class="col-sm-10 col-md-6">
                            <select class="custom-select" name="parent_id">
                              <option value="0">...</option>
                              <?php 
                              $cats=getAllform('*','categories','','','id','ASC');
                              
                              foreach($cats as $cat){
                                echo '<option value="'.$cat['id'].'" '; if($row['parent_id']==$cat['id']){echo "selected ";} echo'>'.$cat['name'].'</option>';
                              }
                              ?>
                            </select>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Visible</label>
                    <div class="col-sm-10 col-md-6">
                        <div>
                            <input id="vis-yes" type="radio" name="visible" value="0"
                            <?php if($row['visibility'] == 0) echo "checked"?> >
                            <label for="vis-yes">Yes</label>
                        </div>
                        <div>
                            <input id="vis-no" type="radio" name="visible" value="1"
                            <?php if($row['visibility'] == 1) echo "checked"?>>
                            <label for="vis-no">No</label>
                        </div>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">AllowComment</label>
                    <div class="col-sm-10 col-md-6">
                        <div>
                            <input id="com-yes" type="radio" name="comment" value="0"
                             <?php if($row['allow_comment'] == 0) echo "checked"?>>
                            <label for="com-yes">Yes</label>
                        </div>
                        <div>
                            <input id="com-no" type="radio" name="comment" value="1"
                            <?php if($row['allow_comment'] == 1) echo "checked"?>>
                            <label for="com-no">No</label>
                        </div>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">AllowAds</label>
                    <div class="col-sm-10 col-md-6">
                        <div>
                            <input id="ads-yes" type="radio" name="ads" value="0"
                            <?php if($row['allow_ads'] == 0) echo "checked"?>>
                            <label for="ads-yes">Yes</label>
                        </div>
                        <div>
                            <input id="ads-no" type="radio" name="ads" value="1"
                            <?php if($row['allow_ads'] == 1) echo "checked"?>>
                            <label for="ads-no">No</label>
                        </div>
                    </div>
                    </div>
                    <div class="form-group row btn-lg">                 
                    <div class="offset-sm-2 col-sm-10 offset-md-4">
                        <input type="submit" value="Update" class="btn btn-primary">
                    </div>
                    </div>
                
            </form>
        </div>
        <?php }else{
        echo "<div class='container'>";
        $msg = "<div class='alert alert-danger'>there is no such id !</div>";
        redirectHome($msg,'back');
        echo "</div>";
    }

    }elseif($page=='update'){ 

        echo "<div class='container'>
        <h1 class='text-center'>UPdate Category</h1>";
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            
            $cat_id     = $_POST['catid'];//hidden input to pass id
            $name       = $_POST['name'];
            $desc       = $_POST['desc'];
            $order      = $_POST['order'];
            $parentid   = $_POST['parent_id'];
            $visible    = $_POST['visible'];
            $allow_comm = $_POST['comment'];
            $allow_ads  = $_POST['ads'];

            
            $formerrors = array();
            if(empty($name))
            {
                $formerrors[] = "name cannt be <strong>empty</strong>.";
            }
           

            foreach($formerrors as $error)
            {
               echo "<div class='alert alert-danger' >".$error."</div>";
            }
            if(empty($formerrors))
            {
                $query = $con->prepare("update categories set name=? ,description=?,parent_id=? ,ordering=? ,visibility=?, allow_comment=?, allow_ads=? where id=?");
                $query->execute(array($name,$desc,$parentid,$order,$visible,$allow_comm,$allow_ads,$cat_id));
                $msg = "<div class='alert alert-success'>update Done </div>";
                redirectHome($msg,'back');
            }
            


        }else{
            $msg = "<div class='alert alert-danger'>you cant view that page </div>";
            redirectHome($msg,'back');
        }
        echo "</div>";


    }elseif($page=='delete'){ ?>

        <h1 class="text-center">Delete Category</h1>
        <div class="container"> <?php
            $catId = isset($_GET['id']) && is_numeric($_GET['id'])? intval($_GET['id']):0;

            $check = checkItem('id' ,'categories' ,$catId);
            if($check >0){
                $query = $con->prepare("delete from categories where id=?");
                $query->execute(array($catId));
                $msg = "<div class='alert alert-success'>row deleted</div>";
                redirectHome($msg,'back');

            }else{
                $msg = "<div class='alert alert-danger'>there is no such id</div>";
                redirectHome($msg,'back');
            } ?>
        </div>

    <?php     
    }

	include $tmp .'footer.php';

}else{

   header('location: index.php');
   exit();
}

?>