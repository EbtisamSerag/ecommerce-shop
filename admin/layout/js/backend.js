$(function() {

	'use strict';

  //home dashboard
  $('.toggle-info').click(function(){

    $(this).toggleClass('selected').parent().next('.card-body').fadeToggle(100);

    if($(this).hasClass('selected')){
      $(this).html('<i class="fa fa-plus"></i>');
    }else{
      $(this).html('<i class="fa fa-minus"></i>');
    }
  });
	//hide placeholder on form focus
	$('input,textarea').focus(function(){
    $(this).data('placeholder',$(this).attr('placeholder'))
          .attr('placeholder','');
	}).blur(function(){
	   $(this).attr('placeholder',$(this).data('placeholder'));
	});
  //trigger the selectboxit
   $("select").selectBoxIt({
      autowidth:false
   });

	//add asterisk on required input
	$('input').each(function(){
       if($(this).attr('required') === 'required')
       {
           $(this).after('<span class="asterisk">*</span>');
       }
	});

  //add asterisk on required textarea 
  $('textarea').each(function(){
       if($(this).attr('required') === 'required')
       {
           $(this).after('<span class="asterisk">*</span>');
       }
  });

    //convert password to text on hover
     var passfield = $('.password');
    $('.show-pass').hover(function(){
       passfield.attr('type','text');
    },function(){
       passfield.attr('type','password');
    }); 

    //confirmation message
    $('.confirm').click(function(){
    	return confirm('Are you sure ?')
    });

	
});