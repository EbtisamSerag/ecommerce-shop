<?php 
/*
===============================
**manage member page 
**you can [add,edit,delete]
===============================
*/
session_start();
$pageTitle="members";
if(isset($_SESSION['user']))
{
	
	include 'init.php';

	$page =isset($_GET['page'])? $_GET['page']:'manage';
	if($page=='manage'){
        
        $q = '';
        if(isset($_GET['pending'])){
            $q = 'AND reg_status = 0';
        }


	    $query = $con->prepare("SELECT * from users where group_id !=1 $q");
        $query->execute();
        $users = $query->fetchAll();
		?>
		<h1 class="text-center">Manage Members</h1>
        <div class="container">
        	<table class="table main-table table-bordered manage-member">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">#Id</th>
                  <th scope="col">Image</th>        
			      <th scope="col">Username</th>
			      <th scope="col">Email</th>
			      <th scope="col">Fullname</th>
			      <th scope="col">Register date</th>
			      <th scope="col">Controll</th>

			    </tr>
			  </thead>
			  <tbody>
			  	<?php
                  foreach ($users as $user) {
			  	?>

			    <tr>
			      <th scope="row"><?php echo $user['user_id']?></th>
                  <td><?php echo '<img src="uploads/avatar/'.$user['image'].'" alt=""/>';?></td>                  
			      <td><?php echo $user['username']?></td>
			      <td><?php echo $user['email']?></td>
			      <td><?php echo $user['fullname']?></td>
			      <td><?php echo $user['date']?></td>
			      <td><a href="?page=edit&id=<?php echo $user['user_id'] ?>" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>
			      	<a href="?page=delete&id=<?php echo $user['user_id'] ?>" class="btn btn-danger confirm"><i class="fa fa-times"></i> Delete</a>

                    <?php if($user['reg_status'] == 0){ ?>
                    <a href="?page=activate&id=<?php echo $user['user_id'] ?>" class="btn btn-info activate"><i class="fa fa-check"></i> Activate</a></td>
                    <?php }?> 
			    </tr>
			<?php }?>
			  </tbody>
			</table>
			<a href="?page=add" class="btn btn-primary"><i class="fa fa-plus"></i> New Member</a>
        </div>
 
    <?php 
    }elseif($page =='add')
    {?>
        <h1 class="text-center">Add Members</h1>
        <div class="container">
        	<form action="?page=insert" method="POST" enctype="multipart/form-data">
        		<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">UserName</label>
        			<div class="col-sm-10 col-md-6">
        				<input type="text" name="username" class="form-control" autocomplete="off" placeholder="username to login shop" required>
        			</div>
        		    </div>
        			<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">Password</label>
        			<div class="col-sm-10 col-md-6">
        				<input type="password" name="password" class="password form-control" autocomplete="new-password" placeholder=" password must be complex" required>
        				<i class="show-pass fa fa-eye "></i>
        			</div>
        		    </div>
        			<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">Email</label>
        			<div class="col-sm-10 col-md-6">
        				<input type="email" name="email" class="form-control" placeholder="email must be valid" required>
        			</div>
        		    </div>
        			<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">FullName</label>
        			<div class="col-sm-10 col-md-6">
        				<input type="text" name="full" class="form-control" placeholder="fullname appear in your profile" required>
        			</div>
        		    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Image</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="file" name="image" class="form-control" required>
                    </div>
                    </div>
        			<div class="form-group row btn-lg">       			
        			<div class="offset-sm-2 col-sm-10 offset-md-4">
        				<input type="submit" value="Add Member" class="btn btn-primary">
        			</div>
        		    </div>
        		
        	</form>
        </div>
        
      
	<?php }
	elseif($page== 'insert')
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			echo "<div class='container'>
    	          <h1 class='text-center'>Add Members</h1>";
			$username = $_POST['username'];
			$password = $_POST['password'];
			$email    = $_POST['email'];
			$fullname = $_POST['full'];

            $img_name    = $_FILES['image']['name'];
            $img_size    = $_FILES['image']['size'];
            $img_tmp     = $_FILES['image']['tmp_name'];
            $img_type    = $_FILES['image']['type'];


            $allowed_extensions = array("jpg","jpeg","png","gif");


			$hashedpass = sha1($_POST['password']);

			$formerrors = array();
            if(empty($username))
            {
            	$formerrors[] = "username cannt be <strong>empty</strong>.";
            }
            if(empty($password))
            {
            	$formerrors[] = "password cannt be <strong>empty</strong>.";
            }
            if(empty($email))
            {
            	$formerrors[] = "email cannt be <strong>empty</strong>.";
            }
            if(empty($fullname))
            {
            	$formerrors[] = "fullname cannt be <strong>empty</strong>.";
            }
            if(empty($img_name))
            {
                $formerrors[] = "image cannt be <strong>empty</strong>.";
            }
            $exten = explode('.',$img_name);
            $img_extension = end($exten);
            if(!empty($img_name) && !in_array( $img_extension, $allowed_extensions)){
                 $formerrors[] = "this extension not <strong>allowed</strong>.";
            }


            foreach($formerrors as $error)
            {
               echo "<div class='alert alert-danger' >".$error."</div>";
            }
            if(empty($formerrors))
            {
                $image = rand(0,100000).'_'.$img_name;
                if(move_uploaded_file($img_tmp,"uploads/avatar/".$image))
                {
            	$check = checkItem('username','users',$username);
            	if($check == 1)
            	{
                    $msg = "<div class='alert alert-danger'>sorry this username is exist </div>";
                    redirectHome($msg,'back');
            	}else{
	            	$query = $con->prepare("insert into users(username,password,email,fullname,image,reg_status,date) values(?,?,?,?,?,1,now())");
		    		$query->execute(array($username,$hashedpass,$email,$fullname,$image));
		    		$msg = "<div class='alert alert-success'>Insert Done </div>";
		    		redirectHome($msg,'back');
	    	   }
               }
            }
        }else{
        	echo "<div class='container'>";
        	$msg = "<div class='alert alert-danger'>you cannt view that page!</div>";
        	redirectHome($msg,'back');
        	echo "</div>";
        }
	}elseif($page=='edit'){//edit page
    $id =intval($_GET['id']) ;
	$userId = isset($_GET['id']) && is_numeric($_GET['id'])?  $id:0;
	$query =$con->prepare("select * from users where user_id =? limit 1");
	$query->execute(array($userId));
	$row =$query->fetch();
	if($query->rowCount()>0){?>
		
        <h1 class="text-center">Edit Members</h1>
        <div class="container">
        	<form action="?page=update" method="POST" enctype="multipart/form-data">
        		<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">UserName</label>
        			<div class="col-sm-10 col-md-6">
        				<input type="text" name="username" value="<?php echo $row['username']?>" class="form-control" autocomplete="off" required>
        				<input type="hidden" name="userid" value="<?php echo $userId?>">
        			</div>
        		    </div>
        			<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">Password</label>
        			<div class="col-sm-10 col-md-6">
        				<input type="hidden" name="oldpassword" value="<?php echo $row['password'] ?>">
        				<input type="password" name="newpassword" class="form-control" autocomplete="new-password" placeholder="Leave Blank if you won't change password">
        			</div>
        		    </div>
        			<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">Email</label>
        			<div class="col-sm-10 col-md-6">
        				<input type="email" name="email" value="<?php echo $row['email']?>" class="form-control" required>
        			</div>
        		    </div>
        			<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">FullName</label>
        			<div class="col-sm-10 col-md-6">
        				<input type="text" name="full" value="<?php echo $row['fullname']?>" class="form-control" required>
        			</div>
        		    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Image</label>
                    <div class="col-sm-10 col-md-6">
                        <img src="uploads/avatar/<?php echo $row['image']?>" alt="" class="avatar-img"/>
                        <input type="hidden" name="img" value="<?php echo $row['image']?>">
                        <input type="file" name="image" class="form-control" >
                    </div>
                    </div>
        			<div class="form-group row btn-lg">       			
        			<div class="offset-sm-2 col-sm-10 offset-md-4">
        				<input type="submit" value="save" class="btn btn-primary">
        			</div>
        		    </div>
        		
        	</form>
        </div>
        

	<?php }else{
		echo "<div class='container'>";
		$msg = "<div class='alert alert-danger'>there is no such id !</div>";
		redirectHome($msg,'back');
		echo "</div>";
	}
    }elseif($page=='update'){

    	echo "<div class='container'>
    	<h1 class='text-center'>UPdate Members</h1>";
    	if($_SERVER['REQUEST_METHOD'] == 'POST'){
    		
    		$id       = $_POST['userid'];
    		$username = $_POST['username'];
    		$email    = $_POST['email'];
    		$fullname = $_POST['full'];


    		$pass = empty($_POST['newpassword'])?$_POST['oldpassword']:sha1($_POST['newpassword']);
    		
            $formerrors = array();
            if(empty($username))
            {
            	$formerrors[] = "username cannt be <strong>empty</strong>.";
            }
            if(empty($email))
            {
            	$formerrors[] = "email cannt be <strong>empty</strong>.";
            }
            if(empty($fullname))
            {
            	$formerrors[] = "fullname cannt be <strong>empty</strong>.";
            }

            foreach($formerrors as $error)
            {
               echo "<div class='alert alert-danger' >".$error."</div>";
            }
            if(empty($formerrors))
            {
                if(!empty($_FILES['image']['name'])){
                    $img_name    = $_FILES['image']['name'];
                    $img_size    = $_FILES['image']['size'];
                    $img_tmp     = $_FILES['image']['tmp_name'];
                    $img_type    = $_FILES['image']['type'];

                    $allowed_extensions = array("jpg","jpeg","png","gif");

                    $exten = explode('.',$img_name);
                    $img_extension = end($exten);
                    if(!in_array( $img_extension, $allowed_extensions)){
                         $formerrors[] = "this extension not <strong>allowed</strong>.";
                    }
                    $image = rand(0,100000).'_'.$img_name;
                    move_uploaded_file($img_tmp,"uploads/avatar/".$image);
                }else{
                    $image = $_POST['img'];
                }

                $q = $con->prepare("SELECT username FROM users WHERE username=? AND user_id !=?");
                $q->execute(array($username,$id));
                $count = $q->rowCount();
                if($count > 0)
                {
                    $msg = "<div class='alert alert-danger'>sorry this username is exist </div>";
                    redirectHome($msg,'back');

                }else{
            	$query = $con->prepare("update users set username=? ,email=? ,fullname=? ,image=? ,password=? where user_id=?");
	    		$query->execute(array($username,$email,$fullname,$image,$pass,$id));
	    		$msg = "<div class='alert alert-success'>update Done </div>";
	    		redirectHome($msg,'back');
                }
            }
    		


    	}else{
    		$msg = "<div class='alert alert-danger'>you cant view that page </div>";
    		redirectHome($msg,'back');
    	}
    	echo "</div>";
    }elseif($page=='delete')
    { ?>  
    	<h1 class="text-center">Delete Members</h1>
        <div class="container"> <?php
	    	$userId = isset($_GET['id']) && is_numeric($_GET['id'])? intval($_GET['id']):0;

			$check = checkItem('user_id' ,'users' ,$userId);
			if($check >0){
		    	$query = $con->prepare("delete from users where user_id=?");
		    	$query->execute(array($userId));
		    	$msg = "<div class='alert alert-success'>row deleted</div>";
		    	redirectHome($msg,'back');

		    }else{
		    	$msg = "<div class='alert alert-danger'>there is no such id</div>";
		    	redirectHome($msg,'back');
		    } ?>
	    </div> <?php
    }elseif($page == 'activate'){?>
        
        <h1 class="text-center">Activate Members</h1>
        <div class="container"> <?php
            $userId = isset($_GET['id']) && is_numeric($_GET['id'])? intval($_GET['id']):0;

            $check = checkItem('user_id' ,'users' ,$userId);
            if($check >0){
                $query = $con->prepare("UPDATE  users SET reg_status =1 where user_id=?");
                $query->execute(array($userId));
                $msg = "<div class='alert alert-success'>Activate Done</div>";
                redirectHome($msg,'back');

            }else{
                $msg = "<div class='alert alert-danger'>there is no such id</div>";
                redirectHome($msg,'back');
            } ?>
        </div> <?php

    }

	include $tmp .'footer.php';

}else{

   header('location: index.php');
   exit();
}

?>