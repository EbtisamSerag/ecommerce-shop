<?php 
session_start();
$pageTitle="dashboard";
if(isset($_SESSION['user']))
{
	include 'init.php';
	$limit = '6' ;
    $latestusers      = getLatest('*','users','user_id',$limit);
    $latestitems      = getLatest('*','items','item_id',$limit);
    $latestcomments   = getLatest('*','comments','comm_id',$limit);
    $latestcategories = getLatest('*','categories','id',$limit);



	?>
	<div class="home-stats">
      <div class="container  text-center">
      	<h1>Dashboard</h1>
      	<div class="row">
      		<div class="col-md-3">
      			<div class="stat st-members">
      			    <div class="info">
	      				<i class="fa fa-users"></i>
		      			Total Members
		      			<span><a href="members.php"><?php echo countItems('user_id','users')?></a></span>
	      		    </div>
      			</div>
      		</div>
      		<div class="col-md-3">
      			<div class="stat st-pending">
      			   <div class="info">
      			   	<i class="fa fa-user-plus"></i>
      			   	Pending Members
	      			<span><a href="members.php?page=manage&pending">
	      				<?php echo checkItem('reg_status' ,'users' ,'0')?></a></span>
	      		    </div>
      			   </div>
      		</div>
      		<div class="col-md-3">
      			<div class="stat st-items">
      				<div class="info">
      					<i class="fa fa-tag"></i>
      					Total Items
      			        <span><a href="items.php"><?php echo countItems('item_id','items')?></a></span>
      				</div>
      		    </div>
      		</div>
      		<div class="col-md-3">
      			<div class="stat st-comments"> 
      				<div class="info">
      					<i class="fa fa-comments"></i>
      					Total Comments
      			        <span><a href="comments.php"><?php echo countItems('comm_id','comments')?></a></span>
      				</div>
      		    </div>
      		</div>
      	</div>
      </div>
    </div>

      <div class="latest">
	      <div class="container ">
	      	<div class="row">
	      		<div class="col-sm-6">
	      			<div class="card">
	      				<div class="card-header">
	      					<i class="fa fa-users"></i> Latest <?php echo $limit ?> Registered Users
	      					<span class="toggle-info float-right"><i class="fa fa-minus"></i></span>
	      				</div>
	      				<div class="card-body">
	      					<ul class="list-group latest-users">
		      					<?php 
		      					if(!empty($latestusers)){
		      					foreach($latestusers as $user){
		      						echo '<Li class="list-group-item d-flex justify-content-between align-items-center">' .$user['username'] .'<span>';
		      						
		      						if($user['reg_status'] == 0){
		      							echo '<a class="btn btn-info activate" href="members.php?page=activate&id='.$user['user_id'].'"><i class="fa fa-check "></i> Activate</a>';
		      						}
		      						echo '<a class="btn btn-success" href="members.php?page=edit&id='.$user['user_id'].'"><i class="fa fa-edit "></i> Edit</a>';
		      						echo '</span></li>' ;
		      					}
		      				    }else{
		      				    	echo '<li class="list-group-item d-flex justify-content-between align-items-center">There is no users to show</li>';
		      				    }
		      				    	?>
	      					</ul> 
	      				</div>
	      			</div>
	      		</div>
	      		<div class="col-sm-6">
	      			<div class="card card-default">
	      				<div class="card-header">
	      					<i class="fa fa-tag"></i> Latest <?php echo $limit ?> Items 
	      					<span class="toggle-info float-right"><i class="fa fa-minus"></i></span>
	      				</div>
	      				<div class="card-body">
	      					<ul class="list-group latest-users">
		      					<?php 
		      					if(!empty($latestitems)){
		      					foreach($latestitems as $item){
		      						echo '<Li class="list-group-item d-flex justify-content-between align-items-center">' .$item['name'] .'<span>';
		      						
		      						if($item['approve'] == 0){
		      							echo '<a class="btn btn-info activate" href="items.php?page=approve&id='.$item['item_id'].'"><i class="fa fa-check "></i> Approve</a>';
		      						}
		      						echo '<a class="btn btn-success" href="items.php?page=edit&id='.$item['item_id'].'"><i class="fa fa-edit "></i> Edit</a>';
		      						echo '</span></li>' ;
		      					}
		      				    }else{
		      				    	echo '<li class="list-group-item d-flex justify-content-between align-items-center">There is no items to show</li>';
		      				    }?>
	      					</ul>
	      				</div>
	      			</div>
	      		</div>
	      	</div>

	      	<div class="row">
	      		<div class="col-sm-6">
	      			<div class="card">
	      				<div class="card-header">
	      					<i class="fa fa-comments"></i> Latest <?php echo $limit ?> Comments
	      					<span class="toggle-info float-right"><i class="fa fa-minus"></i></span>
	      				</div>
	      				<div class="card-body">
	      					<?php 
                            $query = $con->prepare("SELECT comments.* ,users.username FROM comments
			                  INNER JOIN users ON comments.user_id = users.user_id
			                  ORDER BY comm_id DESC LIMIT $limit");
							$query->execute();
							$comments = $query->fetchAll();
							if(!empty($comments)){
							foreach ($comments as $comment) {
								echo '<div class="comment-box">';
								     echo '<a href="members.php?page=edit&id='.$comment['user_id'].'"><span class="u-name">'.$comment['username'].'</a></span>';
								     echo '<p class="comment">'.$comment['comment'].'</p>';
								echo '</div>';
							}
					     	}else{
					     		echo '<span>There is no comment to show</span>';
					     	}
	      					?>
	      					</div>
	      				</div>
	      			</div>

	      		<div class="col-sm-6">
	      			<div class="card card-default">
	      				<div class="card-header">
	      					<i class="fa fa-tag"></i> Latest <?php echo $limit ?> Categories 
	      					<span class="toggle-info float-right"><i class="fa fa-minus"></i></span>
	      				</div>
	      				<div class="card-body">
	      					<ul class="list-group latest-users">
		      					<?php 
		      					if(!empty($latestcategories)){
		      					foreach($latestcategories as $category){
		      						echo '<Li class="list-group-item d-flex justify-content-between align-items-center">' .$category['name'] .'<span>';
		      						
		      						echo '<a class="btn btn-success" href="categories.php?page=edit&id='.$category['id'].'"><i class="fa fa-edit "></i> Edit</a>';
		      						echo '</span></li>' ;
		      					}
		      				    }else{
		      				    	echo '<li class="list-group-item d-flex justify-content-between align-items-center">There is no items to show</li>';
		      				    }?>
	      					</ul>
	      				</div>
	      			</div>
	      		</div>
	      	</div>

	      </div>
      </div>
    
	<?php
	include $tmp .'footer.php';
}else{

   header('location: index.php');
   exit();
}

?>