<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 <div class="container">
  <a class="navbar-brand" href="home.php"><?php echo lang('home')?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#appnav" aria-controls="appnav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="appnav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item "><a class="nav-link" href="categories.php"><?php echo lang('categories')?></a></li>
      <li class="nav-item"><a class="nav-link" href="items.php"><?php echo lang('items')?></a></li>
      <li class="nav-item"><a class="nav-link" href="members.php"><?php echo lang('members')?></a></li>
      <li class="nav-item"><a class="nav-link" href="comments.php"><?php echo lang('comments')?></a></li>

    </ul>

    	<div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Admin
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="../index.php"><?php echo lang('visit_shop')?></a>
          <a class="dropdown-item" href="members.php?page=edit&id=<?php echo $_SESSION['id']?>"><?php echo lang('edit_profile')?></a>
          <a class="dropdown-item" href="#"><?php echo lang('setting')?></a>
          <a class="dropdown-item" href="logout.php"><?php echo lang('logout')?></a>
          
      </div>
   </div>
  </div>
</nav>