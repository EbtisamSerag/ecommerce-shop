<?php
  //title function
  function getTitle()
  {
  	global $pageTitle;
  	if(isset($pageTitle))
  	{
  		echo "$pageTitle";
  	}else{
  		echo "default";
  	}
  }

  //ultimate function to get anything you need
  function getAllform($field,$from,$where=null,$and=null,$orderfield,$order='DESC'){
    global $con;
    $q = $con->prepare("SELECT $field FROM $from $where $and ORDER BY $orderfield $order");
    $q->execute();
    $all = $q->fetchAll();
    return $all;

  }
  /*redirect function v2
  **accept message ,url ,seconds
  **
  */
  function redirectHome($msg,$url=null,$seconds = 3)
  {
     if($url == null)
     {
        $url = 'home.php';
     }else{
        $url = isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !== ''?$_SERVER['HTTP_REFERER'] :'home.php';
     }
     echo $msg;
     echo '<div class="alert alert-info ">You will redirect to '.$url.' in '.$seconds.' seconds</div>';
     header("Refresh: ".$seconds."; url=".$url."");
  }

  //function to chec items in database
   function checkItem($select ,$from ,$value)
   {  
      global $con;
      $q = $con->prepare("SELECT $select FROM $from WHERE $select =?");
      $q->execute(array($value));
      $count = $q->rowCount();
      return $count;
   }

   //function to count number of row
   function countItems($item,$table){
    global $con;
    $q = $con->prepare("SELECT count($item) FROM $table ");
    $q->execute();
    return $q->fetchColumn();
   }

   // function to get latest item 
   function getLatest($select,$from,$order=ASC,$limit=5){
    global $con;
    $q = $con->prepare("SELECT $select FROM $from ORDER BY $order DESC LIMIT $limit");
    $q->execute();
    $rows = $q->fetchAll();
    return $rows;
   }
   //function to getitems
   function getItems($select,$from)
   {
    global $con;
    $q = $con->prepare("SELECT $select FROM $from");
    $q->execute();
    $rows = $q->fetchAll();
    return $rows; 
   }
?>