<?php

$host = 'localhost';
$db   = 'e_shop';
$user = 'root';
$pass = '';
$charset = 'utf8';

$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
try {
     $con = new PDO($dsn, $user, $pass, $options);
     
} catch (\PDOException $e) {
	 echo "failed to connect with database !!";
     throw new \PDOException($e->getMessage(), (int)$e->getCode());

}
?>