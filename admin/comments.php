<?php 
/*
===============================
**manage comments page 
**you can [add,edit,delete]
===============================
*/
session_start();
$pageTitle="comments";
if(isset($_SESSION['user']))
{
	
	include 'init.php';

	$page =isset($_GET['page'])? $_GET['page']:'manage';
	if($page == 'manage'){
		$query = $con->prepare("SELECT comments.* ,items.name , users.username FROM comments
			                  INNER JOIN items ON comments.item_id =items.item_id
			                  INNER JOIN users ON comments.user_id = users.user_id");
		$query->execute();
		$comments = $query->fetchAll();
		?>
		<h1 class="text-center">Manage Comments</h1>
        <div class="container">
        	<table class="table main-table table-bordered ">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">#Id</th>
			      <th scope="col">Comment</th>
			      <th scope="col">Item name</th>
			      <th scope="col">User name</th>
			      <th scope="col">Comment Date</th>
			      <th scope="col">Controll</th>

			    </tr>
			  </thead>
			  <tbody>
			  	<?php
                  foreach ($comments as $comment) {
			  	?>

			    <tr>
			      <th scope="row"><?php echo $comment['comm_id']?></th>
			      <td><?php echo $comment['comment']?></td>
			      <td><?php echo $comment['name']?></td>
			      <td><?php echo $comment['username']?></td>
			      <td><?php echo $comment['comm_date']?></td>

			      <td><a href="comments.php?page=edit&id=<?php echo $comment['comm_id'] ?>" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>
			      	<a href="comments.php?page=delete&id=<?php echo $comment['comm_id'] ?>" class="btn btn-danger confirm"><i class="fa fa-times"></i> Delete</a>

                    <?php if($comment['status'] == 0){ ?>
                    <a href="comments.php?page=approve&id=<?php echo $comment['comm_id'] ?>" class="btn btn-info activate"><i class="fa fa-check"></i> Approve</a></td>
                    <?php }?> 
			    </tr>
			<?php }?>
			  </tbody>
			</table>
        </div>
 
    <?php 
     
    }elseif($page == 'edit'){//edit page
    	$id =intval($_GET['id']) ;
		$commId = isset($_GET['id']) && is_numeric($_GET['id'])?  $id:0;
		$query =$con->prepare("select * from comments where comm_id =? limit 1");
		$query->execute(array($commId));
		$row =$query->fetch();
	if($query->rowCount()>0){?>
		
        <h1 class="text-center">Edit Comment</h1>
        <div class="container">
        	<form action="?page=update" method="POST">
        		<input type="hidden" name="commid" value="<?php echo $commId?>">
        		<div class="form-group row form-control-lg offset-md-2">
        			<label class="col-sm-2 control-label">Comment</label>
        			<div class="col-sm-10 col-md-6">
        				<textarea type="text" name="comment"  class="form-control" autocomplete="off" required><?php echo $row['comment']?></textarea>
        				
        			</div>
        		    </div>
        		    <div class="form-group row btn-lg">       			
        			<div class="offset-sm-2 col-sm-10 offset-md-4">
        				<input type="submit" value="save" class="btn btn-primary">
        			</div>
        		    </div>
        			
        	</form>
        </div>
        

	<?php }else{
		echo "<div class='container'>";
		$msg = "<div class='alert alert-danger'>there is no such id !</div>";
		redirectHome($msg,'back');
		echo "</div>";
	}

    }elseif($page == 'update'){
    	echo "<div class='container'>
    	<h1 class='text-center'>UPdate Members</h1>";
    	if($_SERVER['REQUEST_METHOD'] == 'POST'){
    		
    		$id       = $_POST['commid'];
    		$comment  = $_POST['comment'];
    		
            $formerrors = array();
            if(empty($comment))
            {
            	$formerrors[] = "comment cannt be <strong>empty</strong>.";
            }


            foreach($formerrors as $error)
            {
               echo "<div class='alert alert-danger' >".$error."</div>";
            }
            if(empty($formerrors))
            {
            	$query = $con->prepare("update comments set comment=? where comm_id=?");
	    		$query->execute(array($comment,$id));
	    		$msg = "<div class='alert alert-success'>update Done </div>";
	    		redirectHome($msg,'back');
            }
    		


    	}else{
    		$msg = "<div class='alert alert-danger'>you cant view that page </div>";
    		redirectHome($msg,'back');
    	}
    	echo "</div>";


    }elseif($page == 'delete'){  ?>

    	<h1 class="text-center">Delete Comment</h1>
        <div class="container"> <?php
	    	$commId = isset($_GET['id']) && is_numeric($_GET['id'])? intval($_GET['id']):0;

			$check = checkItem('comm_id' ,'comments' ,$commId);
			if($check >0){
		    	$query = $con->prepare("delete from comments where comm_id=?");
		    	$query->execute(array($commId));
		    	$msg = "<div class='alert alert-success'>row deleted</div>";
		    	redirectHome($msg,'back');

		    }else{
		    	$msg = "<div class='alert alert-danger'>there is no such id</div>";
		    	redirectHome($msg,'back');
		    } ?>
	    </div> <?php

    }elseif($page == 'approve'){ ?>
    
     <h1 class="text-center">Approve Comment</h1>
        <div class="container"> <?php
            $commId = isset($_GET['id']) && is_numeric($_GET['id'])? intval($_GET['id']):0;

            $check = checkItem('comm_id' ,'comments' ,$commId);
            if($check >0){
                $query = $con->prepare("UPDATE  comments SET status =1 where comm_id=?");
                $query->execute(array($commId));
                $msg = "<div class='alert alert-success'>Activate Done</div>";
                redirectHome($msg,'back');

            }else{
                $msg = "<div class='alert alert-danger'>there is no such id</div>";
                redirectHome($msg,'back');
            } ?>
        </div> <?php 

    }
        
	include $tmp .'footer.php';

}else{

   header('location: index.php');
   exit();
}

?>