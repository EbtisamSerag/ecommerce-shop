<?php 
/*
===============================
**manage items page 
**you can [add,edit,delete]
===============================
*/
session_start();
$pageTitle="items";
if(isset($_SESSION['user']))
{
	
	include 'init.php';

	$page =isset($_GET['page'])? $_GET['page']:'manage';
	if($page == 'manage'){ 
        

        $query = $con->prepare("SELECT items.* ,categories.name AS cat_name ,users.username
                                FROM items 
                                INNER JOIN categories ON items.cat_id = categories.id
                                INNER JOIN users ON items.member_id = users.user_id");
        $query->execute();
        $items = $query->fetchAll();

        ?>
        <h1 class="text-center">Manage Items</h1>
        <div class="container">
            <table class="table main-table table-bordered ">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">#Id</th>
                  <th scope="col">name</th>
                  <th scope="col">Description</th>
                  <th scope="col">Price</th>
                  <th scope="col">Date</th>
                  <th scope="col">Country Made</th>
                  <th scope="col">Member</th>
                  <th scope="col">Category</th>
                  <th scope="col">Controll</th>

                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($items as $item) {
                ?>

                <tr>
                  <th scope="row"><?php echo $item['item_id']?></th>
                  <td><?php echo $item['name']?></td>
                  <td><?php echo $item['description']?></td>
                  <td><?php echo $item['price']?></td>
                  <td><?php echo $item['add_date']?></td>
                  <td><?php echo $item['country_made']?></td>
                  <td><?php echo $item['username']?></td>
                  <td><?php echo $item['cat_name']?></td>

                  <td><a href="?page=edit&id=<?php echo $item['item_id'] ?>" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>
                    <a href="?page=delete&id=<?php echo $item['item_id'] ?>" class="btn btn-danger confirm"><i class="fa fa-times"></i> Delete</a>

                    <?php if($item['approve'] == 0){ ?>
                    <a href="?page=approve&id=<?php echo $item['item_id'] ?>" class="btn btn-info "><i class="fa fa-check"></i> Approve</a></td>
                    <?php }?> 

                </tr>
            <?php }?>
              </tbody>
            </table>
            <a href="?page=add" class="btn btn-primary"><i class="fa fa-plus"></i> New Item</a>
        </div>
 
     
     <?php
    }elseif($page == 'add'){?>

    	<h1 class="text-center">Add Items</h1>
        <div class="container">
            <form action="?page=insert" method="POST">
                <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="name" class="form-control" placeholder="enter Item name" required >
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10 col-md-6">
                        <textarea  name="desc" class="form-control" placeholder="enter description of item" required ></textarea>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="price" class="form-control" placeholder="enter ordering price of item" required>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Country</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="country_made" class="form-control" placeholder="enter country of made" required  >
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10 col-md-6">
                            <select class="custom-select" name="status">
							  <option selected>...</option>
							  <option value="1">NEW</option>
							  <option value="2">LIKE NEW</option>
							  <option value="3">OLD</option>
							</select>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Member</label>
                    <div class="col-sm-10 col-md-6">
                            <select class="custom-select" name="member_id">
							  <option selected>...</option>
							  <?php 
							  foreach (getItems('*','users') as $user)
							  	echo "<option value='".$user['user_id']."'>".$user['username']."</option>";
                                
							     ?>
							</select>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-10 col-md-6">
                            <select class="custom-select" name="cat_id">
							  <option selected>...</option>
							  <?php 
							  foreach (getItems('*','categories') as $category)
							  	echo "<option value='".$category['id']."'>".$category['name']."</option>";

							  	?>
							</select>
                    </div>
                    </div>
                    
                    
                    <div class="form-group row btn-lg">                 
                    <div class="offset-sm-2 col-sm-10 offset-md-4">
                        <input type="submit" value="Add Item" class="btn btn-primary">
                    </div>
                    </div>
                
            </form>
        </div>

     <?php
    }elseif($page == 'insert'){

    	if($_SERVER['REQUEST_METHOD'] == 'POST'){
			echo "<div class='container'>
    	          <h1 class='text-center'>Add Items</h1>";
			$name       = $_POST['name'];
			$desc       = $_POST['desc'];
			$price      = $_POST['price'];
			$country    = $_POST['country_made'];
			$status     = $_POST['status'];
			$cat_id     = $_POST['cat_id'];
			$member_id  = $_POST['member_id'];


			$formerrors = array();
            if(empty($name))
            {
            	$formerrors[] = "name cannt be <strong>empty</strong>.";
            }
            if(empty($desc))
            {
            	$formerrors[] = "description cannt be <strong>empty</strong>.";
            }
            if(empty($price))
            {
            	$formerrors[] = "price cannt be <strong>empty</strong>.";
            }
            if(empty($country))
            {
            	$formerrors[] = "country cannt be <strong>empty</strong>.";
            }
            if($status == 0)
            {
            	$formerrors[] = "status cannt be <strong>empty</strong>.";
            }
            if($cat_id == 0)
            {
            	$formerrors[] = "category cannt be <strong>empty</strong>.";
            }
            if($member_id == 0)
            {
            	$formerrors[] = "member cannt be <strong>empty</strong>.";
            }

            foreach($formerrors as $error)
            {
               echo "<div class='alert alert-danger' >".$error."</div>";
            }
            if(empty($formerrors))
            {

            	$query = $con->prepare("insert into items(name,description,price,country_made,status,add_date,cat_id,member_id) values(?,?,?,?,?,now(),?,?)");
	    		$query->execute(array($name,$desc,$price,$country,$status,$cat_id,$member_id));
	    		$msg = "<div class='alert alert-success'>Insert Done </div>";
	    		redirectHome($msg,'back');

            }
        }else{
        	echo "<div class='container'>";
        	$msg = "<div class='alert alert-danger'>you cannt view that page!</div>";
        	redirectHome($msg,'back');
        	echo "</div>";
        }
		
	}elseif($page == 'edit'){//edit page

        $id =intval($_GET['id']) ;
        $itemId = isset($_GET['id']) && is_numeric($_GET['id'])?  $id:0;
        $query =$con->prepare("select * from items where item_id =? limit 1");
        $query->execute(array($itemId));
        $row =$query->fetch();
    if($query->rowCount()>0){?>
        
        <h1 class="text-center">Edit Items</h1>
        <div class="container">
            <form action="?page=update" method="POST">
                <input type="hidden" name="itemid" value="<?php echo $row['item_id'] ?>">
                <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="name" class="form-control" placeholder="enter Item name" required value="<?php echo $row['name']?>" >
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10 col-md-6">
                        <textarea  name="desc" class="form-control" placeholder="enter description of item" required ><?php echo $row['description']?></textarea>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="price" class="form-control" placeholder="enter ordering price of item" value="<?php echo $row['price']?>" required>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Country</label>
                    <div class="col-sm-10 col-md-6">
                        <input type="text" name="country_made" class="form-control" placeholder="enter country of made" required value="<?php echo $row['country_made']?>" >
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10 col-md-6">
                            <select class="custom-select" name="status">
                              <option selected>...</option>
                              <option value="1" <?php if($row['status']==1) echo "selected" ?>>NEW</option>
                              <option value="2" <?php if($row['status']==2) echo "selected" ?>>LIKE NEW</option>
                              <option value="3"<?php if($row['status']==3) echo "selected" ?>>OLD</option>
                            </select>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Member</label>
                    <div class="col-sm-10 col-md-6">
                            <select class="custom-select" name="member_id">
                              <?php 
                              foreach (getItems('*','users') as $user){
                                echo "<option value='".$user['user_id']."'";
                                if($row['member_id']==$user['user_id']) {echo "selected" ;}
                                echo " >".$user['username']."</option>";
                                
                                 }?>
                            </select>
                    </div>
                    </div>
                    <div class="form-group row form-control-lg offset-md-2">
                    <label class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-10 col-md-6">
                            <select class="custom-select" name="cat_id">
                              <?php 
                              foreach (getItems('*','categories') as $category){
                                echo "<option value='".$category['id']."'";
                                if($row['cat_id']==$category['id']) {echo "selected" ;}
                                echo ">".$category['name']."</option>";

                               } ?>
                            </select>
                    </div>
                    </div>
                    
                    
                    <div class="form-group row btn-lg">                 
                    <div class="offset-sm-2 col-sm-10 offset-md-4">
                        <input type="submit" value="Update" class="btn btn-primary">
                    </div>
                    </div>
                
            </form>
            <?php
            $query = $con->prepare("SELECT comments.* , users.username FROM comments
                              INNER JOIN users ON comments.user_id = users.user_id
                              WHERE item_id=?");
                $query->execute(array($row['item_id']));
                $comments = $query->fetchAll();
                if(!empty($comments)){
                ?>
                <h1 class="text-center">Manage [<?php echo $row['name'] ?>] Comments</h1>
                <div class="container">
                    <table class="table main-table table-bordered ">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">Comment</th>
                          <th scope="col">User name</th>
                          <th scope="col">Comment Date</th>
                          <th scope="col">Controll</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          foreach ($comments as $comment) {
                        ?>

                        <tr>
                          <td><?php echo $comment['comment']?></td>
                          <td><?php echo $comment['username']?></td>
                          <td><?php echo $comment['comm_date']?></td>

                          <td><a href="comments.php?page=edit&id=<?php echo $comment['comm_id'] ?>" class="btn btn-success"><i class="fa fa-edit"></i> Edit</a>
                            <a href="comments.php?page=delete&id=<?php echo $comment['comm_id'] ?>" class="btn btn-danger confirm"><i class="fa fa-times"></i> Delete</a>

                            <?php if($comment['status'] == 0){ ?>
                            <a href="comments.php?page=approve&id=<?php echo $comment['comm_id'] ?>" class="btn btn-info activate"><i class="fa fa-check"></i> Approve</a></td>
                            <?php }?> 
                        </tr>
                    <?php }?>
                      </tbody>
                    </table>
                <?php } ?>
        </div>

        
    <?php
        }
    }elseif($page == 'update'){ 

        echo "<div class='container'>
        <h1 class='text-center'>UPdate Items</h1>";
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            
            $id         = $_POST['itemid'];
            $name       = $_POST['name'];
            $desc       = $_POST['desc'];
            $price      = $_POST['price'];
            $country    = $_POST['country_made'];
            $status     = $_POST['status'];
            $cat_id     = $_POST['cat_id'];
            $member_id  = $_POST['member_id'];
            
            $formerrors = array();
            if(empty($name))
            {
                $formerrors[] = "name cannt be <strong>empty</strong>.";
            }
            if(empty($desc))
            {
                $formerrors[] = "description cannt be <strong>empty</strong>.";
            }
            if(empty($price))
            {
                $formerrors[] = "price cannt be <strong>empty</strong>.";
            }
            if(empty($country))
            {
                $formerrors[] = "country cannt be <strong>empty</strong>.";
            }
            if($status == 0)
            {
                $formerrors[] = "status cannt be <strong>empty</strong>.";
            }
            if($cat_id == 0)
            {
                $formerrors[] = "category cannt be <strong>empty</strong>.";
            }
            if($member_id == 0)
            {
                $formerrors[] = "member cannt be <strong>empty</strong>.";
            }

            foreach($formerrors as $error)
            {
               echo "<div class='alert alert-danger' >".$error."</div>";
            }
            if(empty($formerrors))
            {
                $query = $con->prepare("update items set name=? ,description=? ,price=? ,country_made=?,status=?, cat_id=?,member_id=? where item_id=?");
                $query->execute(array($name,$desc,$price,$country,$status,$cat_id,$member_id,$id));
                $msg = "<div class='alert alert-success'>update Done </div>";
                redirectHome($msg,'back');
            }
            


        }else{
            $msg = "<div class='alert alert-danger'>you cant view that page </div>";
            redirectHome($msg,'back');
        }
        echo "</div>";


    }elseif($page == 'delete'){  ?>

        <h1 class="text-center">Delete Item</h1>
        <div class="container"> <?php

            $itemId = isset($_GET['id']) && is_numeric($_GET['id'])? intval($_GET['id']):0;

            $check = checkItem('item_id' ,'items' ,$itemId);
            if($check >0){
                $query = $con->prepare("delete from items where item_id=?");
                $query->execute(array($itemId));
                $msg = "<div class='alert alert-success'>row deleted</div>";
                redirectHome($msg,'back');

            }else{
                $msg = "<div class='alert alert-danger'>there is no such id</div>";
                redirectHome($msg,'back');
            } ?>
        </div> 

     <?php
    }elseif($page == 'approve'){ ?>

        <h1 class="text-center">Approve</h1>
        <div class="container"> <?php
            $approveId = isset($_GET['id']) && is_numeric($_GET['id'])? intval($_GET['id']):0;

            $check = checkItem('item_id' ,'items' ,$approveId);
            if($check >0){
                $query = $con->prepare("UPDATE  items SET approve =1 where item_id=?");
                $query->execute(array($approveId));
                $msg = "<div class='alert alert-success'>Approve Done</div>";
                redirectHome($msg,'back');

            }else{
                $msg = "<div class='alert alert-danger'>there is no such id</div>";
                redirectHome($msg,'back');
            } ?>
        </div> <?php 

    }

	include $tmp .'footer.php';

}else{

   header('location: index.php');
   exit();
}

?>