$(function() {

	'use strict';

  //login/signup page
  $('.login-page h1 span').click(function(){
    $(this).addClass('selected').siblings().removeClass('selected');
    $('.login-page form').hide();
    $('.' + $(this).data('class')).fadeIn(100);
  });
 
	//hide placeholder on form focus
	$('input,textarea').focus(function(){
    $(this).data('placeholder',$(this).attr('placeholder'))
          .attr('placeholder','');
	}).blur(function(){
	   $(this).attr('placeholder',$(this).data('placeholder'));
	});
  //trigger the selectboxit
   $("select").selectBoxIt({
      autoWidth: false 
   });

	//add asterisk on required input
	$('input').each(function(){
       if($(this).attr('required') === 'required')
       {
           $(this).after('<span class="asterisk">*</span>');
       }
	});

  //add asterisk on required textarea 
  $('textarea').each(function(){
       if($(this).attr('required') === 'required')
       {
           $(this).after('<span class="asterisk">*</span>');
       }
  });

    //convert password to text on hover
     var passfield = $('.password');
    $('.show-pass').hover(function(){
       passfield.attr('type','text');
    },function(){
       passfield.attr('type','password');
    }); 

    //confirmation message
    $('.confirm').click(function(){
    	return confirm('Are you sure ?')
    });

    ///live preview in create Ads
    $('.live-name').keyup(function(){
      $('.live-preview .card-title').text($(this).val());
    });
    $('.live-desc').keyup(function(){
      $('.live-preview .card-text').text($(this).val());
    });

    $('.live-price').keyup(function(){
          $('.live-preview .price-tag').text($(this).val());
        });


	
});